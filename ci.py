import sqlite3
from os.path import exists
from configparser import RawConfigParser
from typing import Optional
from pyrogram import Client

# [basic]
BOT_TOKEN: Optional[str] = None
ADMINS = ""
try:
    config = RawConfigParser()
    config.read("config.ini")

    # [basic]
    BOT_TOKEN = config["basic"].get("bot_token")
    ADMINS = config["basic"].get("admins", ADMINS).split(",")
except Exception as e:
    raise RuntimeError(f"Read data from config.ini error: {e}")
# check data.db
if not exists("data.db"):
    raise FileNotFoundError("data.db not found.")
# check data.json
if not exists("data.json"):
    raise FileNotFoundError("data.json not found.")

app = Client("bot", bot_token=BOT_TOKEN)
with app:
    me = app.get_me()
