from pyrogram import Client, emoji
from pyrogram.types import InlineQuery, InlineQueryResultArticle, InputTextMessageContent, InlineKeyboardMarkup, \
    InlineKeyboardButton

from ci import me
from plugins.defs import check_aff_id


@Client.on_inline_query()
async def answer_inline(client: Client, query: InlineQuery):
    aff = 0
    try:
        aff = int(query.query)
    except ValueError:
        await query.answer(
            results=[],
            cache_time=0,
            switch_pm_text=f'{emoji.CROSS_MARK} No aff for "{query.query}"',
            switch_pm_parameter="okay",
        )
    data = check_aff_id(aff)
    if not data:
        await query.answer(
            results=[],
            cache_time=0,
            switch_pm_text=f'{emoji.CROSS_MARK} No aff for "{query.query}"',
            switch_pm_parameter="okay",
        )
    await query.answer(results=[
            InlineQueryResultArticle(
                title="点击邀请 Ta",
                input_message_content=InputTextMessageContent(
                    "点击下方按钮进群"
                ),
                reply_markup=InlineKeyboardMarkup(
                    [
                        [InlineKeyboardButton(
                            "点我点我",
                            url=f"https://t.me/{me.username}?start={aff}"
                        )]
                    ]
                )
            )])
