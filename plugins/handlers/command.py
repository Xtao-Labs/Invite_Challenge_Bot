from pyrogram import Client, filters
from pyrogram.types import Message, InlineKeyboardMarkup, InlineKeyboardButton

from ci import me
from plugins.defs import set_aff, get_aff, send_invite, get_list, get_count

HELP_MSG = """<b>Invite Challenge Bot</b>

/start - 查看此帮助信息
/ping - 我还活着吗？

此项目开源于：https://github.com/Xtao-Labs/Invite_Challenge_Bot"""
AFF_MSG = """请点击下方按钮获取专属邀请链接。"""


@Client.on_message(filters.command("start") & filters.private)
async def start_command(client: Client, message: Message):
    if len(message.command) == 1:
        return await message.reply(HELP_MSG, quote=True)
    if not message.command[1].isnumeric():
        if "get" in message.command[1]:
            try:
                chat_id = int(message.command[1].replace("get", ""))
            except ValueError:
                return
            await get_aff(message, message.from_user.id, chat_id)
        else:
            await message.reply(HELP_MSG, quote=True)
        return
    aff_num = int(message.command[1])
    chat_id = set_aff(message.from_user.id, aff_num)
    if chat_id:
        await send_invite(message, chat_id)


@Client.on_message(filters.command("ping") & filters.private)
async def ping_command(client: Client, message: Message):
    await message.reply("pong~", quote=True)


@Client.on_message(filters.command(["aff", f"aff@{me.username}"]) & filters.group)
async def aff_command(client: Client, message: Message):
    await message.reply(AFF_MSG, reply_markup=InlineKeyboardMarkup(
            [
                [InlineKeyboardButton("点击申请", url=f"https://t.me/{me.username}?start=get{message.chat.id}")]
            ]))


@Client.on_message(filters.command(["affs", f"affs@{me.username}"]) & filters.group)
async def aff_list_command(client: Client, message: Message):
    if not message.from_user:
        return await message.reply("请先解除匿名模式。")
    data = get_list(message.chat.id)
    count = get_count(message.chat.id, message.from_user.id)
    if not data:
        return await message.reply("没有任何人邀请过人。")
    text = []
    for i in range(min(10, len(data))):
        text.append(f"{i + 1}. <code>{data[i][0]}</code> (<code>{data[i][1]}</code>人)")
    await message.reply(f"[您](tg://user?id={message.from_user.id})的邀请数为：<spoiler>{count}</spoiler>\n\n"
                        f"本群 AFF 排行如下：\n\n" + "\n".join(text))
