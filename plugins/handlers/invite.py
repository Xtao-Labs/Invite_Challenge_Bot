from pyrogram import Client, filters, ContinuePropagation
from pyrogram.types import ChatMemberUpdated, ChatJoinRequest, InlineKeyboardMarkup, InlineKeyboardButton

from plugins.defs import group_check, gen_link, invoke_link, invite_check, add_invite

from ci import me

START_MSG = """感谢您邀请我加入群组，我是 Invite Challenge Bot ，能够帮助您统计群组邀请数，**请先赋予我邀请用户权限以继续。**"""
ADMIN_MSG = """恭喜！我已经可以开始统计邀请数了。请需要邀请用户的成员点击下方按钮获取专属邀请链接。

同样你也可以发送 /aff 来生成此消息。"""
UNADMIN_MSG = """呜呜呜 我已被撤销邀请用户权限，真的不要我了吗？"""
PUBLIC_MSG = """呜呜呜 公开群暂不支持此机器人。"""


@Client.on_chat_member_updated()
async def admin_get(client: Client, update: ChatMemberUpdated):
    if update.chat.username:
        invoke_link(update.chat.id)
        await client.send_message(update.chat.id, PUBLIC_MSG)
        await client.leave_chat(update.chat.id)
        return
    if not update.new_chat_member:
        if update.old_chat_member.user.id == me.id:
            invoke_link(update.chat.id)
        return
    if not update.old_chat_member:
        if update.new_chat_member.user.id == me.id:
            await client.send_message(update.chat.id, START_MSG)
        return
    if update.new_chat_member.can_invite_users and (not update.old_chat_member.can_invite_users) and \
            update.new_chat_member.user.id == me.id:
        await gen_link(client, update)
        await client.send_message(update.chat.id, ADMIN_MSG, reply_markup=InlineKeyboardMarkup(
            [
                [InlineKeyboardButton("点击申请", url=f"https://t.me/{me.username}?start=get{update.chat.id}")]
            ]))
    elif (not update.new_chat_member.can_invite_users) and update.old_chat_member.can_invite_users and \
            update.new_chat_member.user.id == me.id:
        invoke_link(update.chat.id)
        await client.send_message(update.chat.id, UNADMIN_MSG)


@Client.on_chat_join_request()
async def apply_aff(client: Client, request: ChatJoinRequest):
    if not group_check(request.chat.id):
        return
    data = await invite_check(client, request)
    if not data:
        return
    await client.approve_chat_join_request(request.chat.id, request.from_user.id)
    add_invite(request.chat.id, request.from_user.id)
