import sqlite3
import json

from pyrogram import Client
from pyrogram.types import Message, InlineKeyboardMarkup, InlineKeyboardButton, ChatMemberUpdated, ChatJoinRequest

from ci import me


def check_aff_id(aff: int):
    conn = sqlite3.connect("data.db")
    cursor = conn.cursor()
    cursor.execute("select * from aff where id=?", (aff,))
    data_ = cursor.fetchone()
    conn.close()
    return data_


def set_aff(uid: int, aff: int):
    data_ = check_aff_id(aff)
    if not data_:
        return
    with open("data.json", "r") as f:
        data = json.load(f)
    data[str(uid)] = aff
    with open("data.json", "w") as f:
        json.dump(data, f)
    return data_[2]


def check_group(cid: int, uid: int):
    with open("data.json", "r") as f:
        data = json.load(f)
    try:
        data_ = data[str(cid)]
    except KeyError:
        data_ = []
    if uid in data_:
        return True
    return False


def add_to_group(cid: int, uid: int):
    with open("data.json", "r") as f:
        data = json.load(f)
    try:
        data_ = data[str(cid)]
    except KeyError:
        data_ = []
    if uid not in data_:
        data_.append(uid)
        data[str(cid)] = data_
        with open("data.json", "w") as f:
            json.dump(data, f)


def remove_aff(uid: int):
    with open("data.json", "r") as f:
        data = json.load(f)
    try:
        del data[str(uid)]
    except KeyError:
        return
    with open("data.json", "w") as f:
        json.dump(data, f)


def check_aff(uid: int):
    with open("data.json", "r") as f:
        data = json.load(f)
    try:
        return data[str(uid)]
    except KeyError:
        return False


def group_check(cid: int):
    conn = sqlite3.connect("data.db")
    cursor = conn.cursor()
    cursor.execute("select * from link where cid=? and status=?", (cid, "active",))
    data = cursor.fetchone()
    conn.close()
    if data:
        return data
    return False


async def get_aff(message: Message, uid: int, cid: int):
    data = group_check(cid)
    if not data:
        return
    conn = sqlite3.connect("data.db")
    cursor = conn.cursor()
    cursor.execute("select * from aff where uid=? and cid=?", (uid, cid,))
    data_ = cursor.fetchone()
    conn.close()

    if not data_:
        conn = sqlite3.connect("data.db")
        cursor = conn.cursor()
        cursor.execute("INSERT INTO aff VALUES (NULL,?,?)", (uid, cid))
        conn.commit()
        cursor.execute("select * from aff where uid=? and cid=?", (uid, cid,))
        data_ = cursor.fetchone()
        conn.close()
    aff = data_[0]
    await message.reply(f"您在当前群组的专属邀请链接是：https://t.me/{me.username}?start={aff}", quote=True,
                        reply_markup=InlineKeyboardMarkup(
                            [
                                [InlineKeyboardButton("分享给好友", switch_inline_query=f"{aff}")]
                            ])
                        )
    return


async def send_invite(message: Message, cid: int):
    data = group_check(cid)
    if not data:
        return
    conn = sqlite3.connect("data.db")
    cursor = conn.cursor()
    cursor.execute("select * from link where cid=? and status=?", (cid, "active"))
    data_ = cursor.fetchone()
    conn.close()
    if not data_:
        await message.reply("暂无可用的邀请链接。", quote=True)
    else:
        await message.reply("请点击下方按钮申请入群。", reply_markup=InlineKeyboardMarkup(
            [
                [InlineKeyboardButton("点击入群", url=data_[1])]
            ]), quote=True)


async def gen_link(client: Client, update: ChatMemberUpdated):
    conn = sqlite3.connect("data.db")
    cursor = conn.cursor()
    cursor.execute("select * from link where cid=?", (update.chat.id,))
    data_ = cursor.fetchone()
    conn.close()
    if data_:
        conn = sqlite3.connect("data.db")
        cursor = conn.cursor()
        cursor.execute("update link set status = 'active' where cid=?", (update.chat.id,))
        conn.commit()
        conn.close()
    else:
        data = await client.create_chat_invite_link(update.chat.id, name="Bot", creates_join_request=True)
        conn = sqlite3.connect("data.db")
        cursor = conn.cursor()
        cursor.execute("INSERT INTO link VALUES (?,?,?)", (update.chat.id, data.invite_link, "active"))
        conn.commit()
        conn.close()


def invoke_link(cid: int):
    conn = sqlite3.connect("data.db")
    cursor = conn.cursor()
    cursor.execute("select * from link where cid=?", (cid,))
    data_ = cursor.fetchone()
    conn.close()
    if data_:
        conn = sqlite3.connect("data.db")
        cursor = conn.cursor()
        cursor.execute("update link set status = 'stop' where cid=?", (cid,))
        conn.commit()
        conn.close()


async def invite_check(client: Client, request: ChatJoinRequest):
    data = group_check(request.chat.id)
    if not data:
        return False
    link = data[1]  # noqa

    if request.invite_link.invite_link != link:
        return False
    if not check_aff(request.from_user.id):
        await client.decline_chat_join_request(request.chat.id, request.from_user.id)
        return False
    return True


def add_invite(cid: int, uid: int):
    aff = check_aff(uid)
    remove_aff(uid)
    data = check_aff_id(aff)
    if check_group(cid, uid):
        return
    add_to_group(cid, uid)
    uid = data[1]

    conn = sqlite3.connect("data.db")
    cursor = conn.cursor()
    cursor.execute("select * from count where uid=? and cid=?", (uid, cid))
    data_ = cursor.fetchone()
    conn.close()
    if data_:
        conn = sqlite3.connect("data.db")
        cursor = conn.cursor()
        cursor.execute("update count set count=? where uid=? and cid=?", (data_[2] + 1, uid, cid))
        conn.commit()
        conn.close()
    else:
        conn = sqlite3.connect("data.db")
        cursor = conn.cursor()
        cursor.execute("INSERT INTO count VALUES (?,?,?)", (uid, cid, 1))
        conn.commit()
        conn.close()


def get_list(cid: int):
    conn = sqlite3.connect("data.db")
    cursor = conn.cursor()
    cursor.execute("select * from count where cid=?", (cid,))
    data_ = cursor.fetchall()
    conn.close()
    data = []
    if data_:
        data = {}
        for i in data_:
            if i[2] > 0:
                data[i[0]] = i[2]
        # 排序
        data = sorted(data.items(), key=lambda x: x[1], reverse=True)
    return data


def get_count(cid: int, uid: int):
    conn = sqlite3.connect("data.db")
    cursor = conn.cursor()
    cursor.execute("select * from count where uid=? and cid=?", (uid, cid))
    data_ = cursor.fetchone()
    conn.close()
    if data_:
        return data_[2]
    return 0
